{
  description = "Embedding Fennel modules in Haskell";

  inputs = {
    haskellNix.url = "github:input-output-hk/haskell.nix";
    nixpkgs.follows = "haskellNix/nixpkgs-unstable";
    flake-utils.follows = "haskellNix/flake-utils";
  };

  outputs = { nixpkgs, flake-utils, haskellNix, ... }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        overlay = final: prev: {
          hsPkgs = final.haskell-nix.project' {
            src = ./.;
            compiler-nix-name = "ghc926";
            shell = {
              tools = {
                cabal = { };
                cabal-fmt = { };
                fourmolu = { };
                ghcid = { };
                haskell-language-server = { };
                hlint = { };
                hoogle = { };
                tasty-discover = { };
              };

              # Non-Haskell stools.
              buildInputs = with pkgs; [ ];
            };
          };
        };
        overlays = [ haskellNix.overlay overlay ];
        pkgs = builtins.foldl' (acc: overlay: acc.extend overlay)
          nixpkgs.legacyPackages.${system} overlays;
        flake = pkgs.hsPkgs.flake { };
      in flake // { defaultPackage = flake.packages."hsfnl:exe:hsfnl"; });
}
