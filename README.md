# hsfnl

Playing with embedding Fennel modules in Haskell project.

# Building and running

To build and run the project, use:

```bash
nix run .#hsfnl:exe:hsfnl
```

To only build the project, use:

```bash
nix build
```

To show all Flake outputs, you can use:

```bash
nix flake show --option allow-import-from-derivation true
```

# Updating Fennel version

Extract `fennel.lua` from the release tarball and place it in the repository
root. Directly downloading `fennel-1.3.0` from https://fennel-lang.org/downloads
and renaming that to `fennel.lua`, will lead to errors.
