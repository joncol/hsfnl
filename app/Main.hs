{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}

import Control.Monad (void)
import Data.ByteString (ByteString)
import Data.FileEmbed
import Data.Version (makeVersion)
import HsLua
import Prelude

registerHaskellModule :: Lua ()
registerHaskellModule =
  preloadModule
    Module
      { moduleName = "hslua"
      , moduleDescription = "Functions from Haskell"
      , moduleFields = []
      , moduleFunctions = [factorial]
      , moduleOperations = []
      , moduleTypeInitializers = []
      }

-- | Factorial function.
factorial :: DocumentedFunction e
factorial =
  defun "factorial"
    ### liftPure (\n -> product [1 .. n] :: Prelude.Integer)
    <#> parameter peekIntegral "integer" "n" "input number"
    =#> functionResult pushIntegral "integer|string" "factorial of n"
    #? "Computes the factorial of an integer."
    `since` makeVersion [1, 0, 0]

-- | Load our embedded copy of @fennel.lua@ and register it in
-- @package.searchers@, then run "main.fnl".
registerFennelAndCallMainFnl :: Lua ()
registerFennelAndCallMainFnl =
  do
    preloadhs "fennel" $ do
      void $ dostring fennelLua
      pure $ NumResults 1
    void
      . dostring
      $ mconcat
        [ "local fennel = require('fennel').install()\n"
        , "return fennel.dofile('main.fnl')\n"
        ]
  where
    fennelLua :: ByteString
    fennelLua = $(embedFile "fennel.lua")

main :: IO ()
main = do
  luaRes <- run @HsLua.Exception $ do
    openlibs
    registerHaskellModule
    registerFennelAndCallMainFnl
    popValue

  print @[Int] luaRes
