(local hs (require :hslua))

(local factorials [])

(for [i 1 10]
  (table.insert factorials (hs.factorial i)))

{ :factorials factorials }
